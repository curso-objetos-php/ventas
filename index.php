<?php

require __DIR__ . '/vendor/autoload.php';

// Importación de las librerías de Monolog
use Monolog\Level;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;


// Creamos y configuramos el logger
$log = new Logger('VentasLogger');
$log->pushHandler(new StreamHandler(__DIR__ . '/logs/application.log', Level::Debug));
$log->pushHandler(new FirePHPHandler());

// Enviamos un mensaje de prueba...
$log->debug('Este mensaje se va al log');

$log->error('Mensaje de error pero de prueba...');

?>

<h1>Sistema de ventas</h1>